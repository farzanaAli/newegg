package pageobjects;

public class objOfHomePage {

    //LogInLink from Homepage
    public static String loginLinkXpath = "//li[@id='usaSite']//a";


    //SearchTextBoxLinks from Homepage

    public static String searchTextBoxXpath = "//input[@id='haQuickSearchBox']";

    public static String searchButtonXpath = "//*[@id='haFormQuickSearch']/div/div[3]/button";

    //Homepage Links
    public static String headerLogoXpath = "/html/body/header/div/div[1]/a/img";

    public static String neweggBussinessXpath = "/html/body/header/div/div[2]/ul/li[1]/a/img";

    public static String dealsAndServicesXpath = "/html/body/header/nav/div/div/div[1]/div/div/a";
        //dropdowns
    public static String neweggmobileLinkXpath = "/html/body/header/nav/div/div/div[1]/div/div/div/div/div/div[1]/div/h3[1]/a";

    public static String emailDealsXpath = "/html/body/header/nav/div/div/div[1]/div/div/div/div/div/div[1]/div/h3[2]/a";

    public static String dailyDealxPpath = "/html/body/header/nav/div/div/div[1]/div/div/div/div/div/div[1]/div/h3[3]/a";

    public static String markerPlaceSpotlightXpath = "/html/body/header/nav/div/div/div[1]/div/div/div/div/div/div[4]/div/h3[4]/a";

    public static String outletXPath = "/html/body/header/nav/div/div/div[1]/div/div/div/div/div/div[1]/div/h3[5]/a";


}
