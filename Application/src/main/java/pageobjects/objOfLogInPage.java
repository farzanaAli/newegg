package pageobjects;

public class objOfLogInPage {

    //LogIn Link is  on objOfHomeGage

    //username box from login page
    public static String loginUserBoxXpath = "//*[@id=\"UserName\"]";

    //password box from login page
    public static String loginPassBoxXpath = "//*[@id=\"UserPwd\"]";

    //Sign in button from login page
    public static String SignInButtonXpath = "//*[@id=\"submitLogin\"]";

    //Sign Up button from login page
    public static String SignUpLinkXpath = "/html/body/div[3]/section/div/div[1]/div/div/span/a/span";

    //First Name box from sign up page
    public static String FirstNameBoxXpath = "//*[@id=\"firstName\"]";

    //Last Name box from sign up page
    public static String LastNameBoxXpath = "//*[@id=\"lastName\"]";

    //email box from sign up page
    public static String signUpEmailBoxXpath = "//*[@id=\"emailAddress\"]";

    //password box from sign up page
    public static String signUpPassBoxXpath = "//*[@id=\"setPwd\"]";

    //Sign up button from sign up page
    public static String SignUpButtonXpath = "//*[@id=\"submitRegistration\"]";

    //Clickable name of user to access drop down menu
    public static String userDropdown = "//*[@id=\"usaSite\"]/a/ins"; //*[@id="usaSite"]/a/ins

    //Logout button
    public static String logoutButton = "//*[@id=\"usaSite\"]/div/div/ul/li[4]/a";

    //Successful logout message
    public static String logoutMessageXpath = "//*[@id=\"infoContent\"]/span/p[2]";

    //Forgot password link
    public static String forgotPassXpath = "//*[@id=\"loginForm\"]/div[2]/div/div[2]/span/a/span";

    //email input for forgotten password
    public static String forgotEmailBoxXpath = "//*[@id=\"frmLogAssist\"]/div[1]/div/input";

    //captcha button
    public static String captchaCheckBocXpath = "//*[@id=\"recaptcha-anchor\"]/div[5]";

    //Submit button for "Forgot Password" page
    public static String forgotSubmitButtonXpath = "//*[@id=\"btn_forgot_pwd\"]";

    //Incorrect password message
    public static String incorrectPassMessageXpath = "//*[@id='alert']";
}
