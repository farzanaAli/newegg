package pageobjects;

public class objOfCustomerService {

    //customer service menu option XPaths
    public static String customerServiceXPath = "/html/body/header/div/nav/ul/li[5]/a/ins";
    public static String trackAnOrderXPath = "//*[@id=\"headerTrackOrder\"]";
    public static String findInvoiceXPath = "//*[@id=\"findInvoice\"]";
    public static String returnItemXPath = "//*[@id=\"headerReturnItem\"]";
    public static String returnStatusXPath = "//*[@id=\"checkReturnStatus\"]";
    public static String findRebatesXPath = "/html/body/header/div/nav/ul/li[5]/div/div/ul/li[5]/a";

    //Return policy is located at the bottom menu for customer service
    public static String returnPolicyXPath = "/html/body/footer/nav[2]/div/ul[1]/li[5]/a";

    //text
    public static String returnPolicyText = "//*[@id=\"standard-return-policy\"]";
    public static String endOfReturnPolicy = "/html/body/div[1]/div[4]/div/div/article/div[1]/p[7]";

    //Find Invoice
    public static String filterMenu = "//*[@id=\"selectOrderTimeRegion\"]";
    public static String ninetyDays = "//*[@id=\"selectOrderTimeRegion\"]/option[3]";

    //Rebate search

    public static String areaRug = "//*[@id=\"bodyCenterArea\"]/div[1]/form/table/tbody/tr[2]/td[2]/select/option[54]";
    public static String instantRebate = "//*[@id=\"bodyCenterArea\"]/div[1]/form/table/tbody/tr[1]/td[2]/select/option[2]";


}