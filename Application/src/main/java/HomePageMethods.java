
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;


import org.testng.Assert;
import pageobjects.objOfHomePage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;



public class HomePageMethods extends Utility {


    public void captureNeweggLogoElement() throws InterruptedException {
        String logoNamePath = getTextByXpath(objOfHomePage.headerLogoXpath);
        System.out.println("Logo Name path: " + logoNamePath);
        takeScreenshotOfElement(objOfHomePage.headerLogoXpath,"neweggLogo_screenshot","C:\\Users\\Wifey\\Dropbox\\Newegg\\Application\\src\\test\\ScreenshotsOrImages\\HomepageElementImage_");
    }

    public void testNeweggBusinessLink() throws InterruptedException {
        clickByXpath(objOfHomePage.neweggBussinessXpath);
        Set<String> ids = driver.getWindowHandles();
        Iterator<String> it = ids.iterator();
        String parentid = it.next();
        String childid = it.next();
        driver.switchTo().window(childid);
        System.out.println("newegg Businness page Title : " + driver.getTitle());
        driver.switchTo().window(parentid);
    }

    public void testDealsAndServicesTab() throws InterruptedException {

        try {
            String expectedTabName = " Deals $ Services";
            String actualTabName = (driver.findElement(By.xpath(objOfHomePage.dealsAndServicesXpath))).getText();

            if (actualTabName == expectedTabName) {
                System.out.println(actualTabName == expectedTabName);
            } else {
                take_screenshot("_ " + actualTabName + "_" + getTodaysDate());
            }
        } catch (Exception e) {
            System.out.println("No such Element found e" + e);
        }
    }

    public void testDropDownNeweggMobileLink() throws InterruptedException {
        clickByXpath(objOfHomePage.dealsAndServicesXpath);
        clickByXpath(objOfHomePage.neweggmobileLinkXpath);
        String urlPage = driver.getCurrentUrl();
        System.out.println("The url of the Mobile Page is : " + urlPage);
        driver.navigate().back();

    }

    public void assertDropDownEmailDealsLinks() throws InterruptedException {
        clickByXpath(objOfHomePage.dealsAndServicesXpath);
        WebElement button_link = driver.findElement(By.linkText("Email Deals"));
        String emailDealsPath = getTextByXpath(objOfHomePage.emailDealsXpath);
        // String button_link= getByAttributes(LinkText("Email Deals"));
        try {
            Assert.assertEquals(button_link.getText(), "Email Deals");
        } catch (AssertionError e) {
            System.out.println("Not equal");
            throw e;
        }
        System.out.println("Equal");


        System.out.println(button_link + ":" + emailDealsPath);

}

    public void testDailyDealsLink() throws InterruptedException {
        clickByXpath(objOfHomePage.dealsAndServicesXpath);
        clickByXpath(objOfHomePage.dailyDealxPpath);
        String title = driver.getTitle();
        driver.navigate().back();
        System.out.println("Title of page : " + title);


    }

    //  public void takeDataFromCSVsheet()throws InterruptedException{}

    public void testDropdownElementOfDealsAndServices() throws InterruptedException {
        String[] actualTextLink = new String[6];
        String[] expectedLinkText = new String[6];
        expectedLinkText[1] = objOfHomePage.neweggmobileLinkXpath;
        expectedLinkText[2] = objOfHomePage.headerLogoXpath;
        expectedLinkText[3] = objOfHomePage.dailyDealxPpath;
        expectedLinkText[4] = objOfHomePage.markerPlaceSpotlightXpath;
        expectedLinkText[5] = objOfHomePage.outletXPath;


        for (int i = 1; i < 6; i++) {
            clickByXpath(objOfHomePage.dealsAndServicesXpath);
            actualTextLink[i] = clickXpath("/html/body/header/nav/div/div/div[1]/div/div/div/div/div/div[1]/div/h3[" + i + "]/a");
            if (expectedLinkText[i].equals(actualTextLink[i])) {
                actualTextLink[i] = driver.getCurrentUrl();
                System.out.println("Xpath Links of Page URL that matches : " + actualTextLink[i]);
            } else {
                actualTextLink[i] = driver.getCurrentUrl();
                System.out.println("Xpath Links  of Page URL that doesn't match : " + actualTextLink[i]);
            }

            driver.navigate().back();

        }

}

}





