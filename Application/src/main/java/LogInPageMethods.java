import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import pageobjects.objOfHomePage;
import pageobjects.objOfLogInPage;

import java.io.IOException;

public class LogInPageMethods extends Utility{
    //6 test
    String dataFilePath="C:\\Users\\Wifey\\Dropbox\\Newegg\\Application\\src\\test\\Data\\DDT-Newegg.xlsx";

    public  String userName = readFromExcel(dataFilePath, "DDT-Newegg", "B5");
    public  String passWord= readFromExcel(dataFilePath, "DDT-Newegg", "B6");

    public LogInPageMethods()throws IOException {}

    public void verifyLoginLink() throws InterruptedException {
       String login = clickXpath(objOfHomePage.loginLinkXpath);
       System.out.println(login);
       driver.navigate().back();
    }

    public void goToLogin() throws InterruptedException {
        String login = clickXpath(objOfHomePage.loginLinkXpath);
        System.out.println(login);
    }


    public void testLoginBox() throws InterruptedException {
        clickByXpath(objOfHomePage.loginLinkXpath);
        typeTextByXpath(objOfLogInPage.loginUserBoxXpath, userName);
        typeTextByXpath(objOfLogInPage.loginPassBoxXpath, passWord);
    }

    public void testLoginButton() throws InterruptedException{
        String click = clickXpath(objOfLogInPage.SignInButtonXpath);
        System.out.println(click);

    }

    public void testLogoutButton() throws InterruptedException {


    try {
        clickByXpath(objOfLogInPage.userDropdown);
        String text= getTextByXpath(objOfLogInPage.logoutButton);
      Assert.assertEquals(text, "//*[@id=\"usaSite\"]/div/div/ul/li[4]/a");
        System.out.println(text);

    }catch(Exception e){
        System.out.println("There's a bug in the page");
    }

//        String logoutClick = clickByXpath(objOfLogInPage.logoutButton);
//        System.out.println(logoutClick);
//
//        try{
//            Assert.assertEquals(getTextByXpath(objOfLogInPage.logoutMessageXpath),"You have successfully logged out.");
//        }catch (AssertionError  e){
//            System.out.println("Logout not complete");
//            throw e;
//        }
//        System.out.println("Logout successful");
//        String expectedMessage = "You are have successfully logged out.";
//        String actualMessage = getTextByXpath(objOfLogInPage.logoutMessageXpath);
//
//        if(expectedMessage == actualMessage)
//            System.out.println("Logout successful");
//
//            clickByXpath(objOfLogInPage.homePageLinkXpath);
    }

    public void verifyForgottenPasswordFunction() throws InterruptedException {
        clickByXpath(objOfHomePage.loginLinkXpath);
        clickByXpath(objOfLogInPage.forgotPassXpath);
        typeTextByXpath(objOfLogInPage.forgotEmailBoxXpath, userName);
        clickByXpath(objOfLogInPage.captchaCheckBocXpath);
        clickByXpath(objOfLogInPage.forgotSubmitButtonXpath);
    }

    public void verifySignUpFunction() throws InterruptedException {
        clickByXpath(objOfHomePage.loginLinkXpath);
        clickByXpath(objOfLogInPage.SignUpLinkXpath);
        typeTextByXpath(objOfLogInPage.FirstNameBoxXpath, "Name");
        typeTextByXpath(objOfLogInPage.LastNameBoxXpath, "Test");
        typeTextByXpath(objOfLogInPage.signUpEmailBoxXpath, "azwadk12@yahoo.com"); //May have to change email for every run
        typeTextByXpath(objOfLogInPage.signUpPassBoxXpath, passWord);
        clickByXpath(objOfLogInPage.SignUpButtonXpath);
        take_screenshot( "newAccount_" + getTodaysDate());
        Thread.sleep(4000);
    }

    public void testLoginFailure() throws InterruptedException{
        clickByXpath(objOfHomePage.loginLinkXpath);
        typeTextByXpath(objOfLogInPage.loginUserBoxXpath, userName);
        typeTextByXpath(objOfLogInPage.loginPassBoxXpath, "test@1234");
        clickByXpath(objOfLogInPage.SignInButtonXpath);
        System.out.println(getTextByXpath(objOfLogInPage.incorrectPassMessageXpath));
    }
    public void getDataFromExcel()throws InterruptedException {
        goToLogin();
        clickByXpath(objOfHomePage.loginLinkXpath);
        typeTextByXpath(objOfLogInPage.loginUserBoxXpath, userName);
        typeTextByXpath(objOfLogInPage.loginPassBoxXpath, passWord);
        System.out.println(userName); System.out.println(passWord);

    }

}
