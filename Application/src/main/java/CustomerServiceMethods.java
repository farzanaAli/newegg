import org.testng.Assert;
import pageobjects.objOfCustomerService;
import org.openqa.selenium.*;
import pageobjects.objOfLogInPage;

import java.io.IOException;

public class CustomerServiceMethods extends Base{

    String dataFilePath="C:\\Users\\Wifey\\Dropbox\\Newegg\\Application\\src\\test\\Data\\DDT-Newegg.xlsx";
    public  String userName = readFromExcel(dataFilePath, "DDT-Newegg", "B5");
    public  String passWord= readFromExcel(dataFilePath, "DDT-Newegg", "B6");


    public CustomerServiceMethods()throws IOException {}

    public void signIn() throws InterruptedException {
        typeTextByXpath(objOfLogInPage.loginUserBoxXpath, userName);
        Thread.sleep(2000);
        typeTextByXpath(objOfLogInPage.loginPassBoxXpath, passWord);
        Thread.sleep(2000);
        clickByXpath(objOfLogInPage.SignInButtonXpath);
    }

    public void signOut() throws InterruptedException {
        clickByXpath(objOfLogInPage.userDropdown);
        clickByXpath(objOfLogInPage.logoutButton);
    }

    public void trackOrder() throws InterruptedException {
        clickByXpath(objOfCustomerService.customerServiceXPath);
        clickByXpath(objOfCustomerService.trackAnOrderXPath);
    }

    public void findInvoice() throws InterruptedException {
        clickByXpath(objOfCustomerService.customerServiceXPath);
        clickByXpath(objOfCustomerService.findInvoiceXPath);
    }

    public void invoiceOptions() throws InterruptedException {
        clickByXpath(objOfCustomerService.filterMenu);
        clickByXpath(objOfCustomerService.ninetyDays);
    }

    public void returnItem() throws InterruptedException {
        clickByXpath(objOfCustomerService.customerServiceXPath);
        clickByXpath(objOfCustomerService.returnItemXPath);
    }

    public void returnStatus() throws InterruptedException {

        clickByXpath(objOfCustomerService.customerServiceXPath);
        clickByXpath(objOfCustomerService.returnStatusXPath);
    }

    public void returnSAssert() throws InterruptedException {
        String noOrderMsg = driver.findElement(By.xpath("//*[@id=\"infoContent\"]/div/div")).getText();
        try{
            Assert.assertEquals(noOrderMsg,"  No results were found within the period specified. Please adjust the date range or search criteria and try again.");
        }catch (Error  e){
            System.out.println("Order Placed");
            throw e;
        }
        System.out.println("No orders found");
    }

    public void findRebates() throws InterruptedException {
        clickByXpath(objOfCustomerService.customerServiceXPath);
        clickByXpath(objOfCustomerService.findRebatesXPath);
    }

    public void rebateOptions() throws InterruptedException {
        clickByXpath(objOfCustomerService.instantRebate);
        clickByXpath(objOfCustomerService.areaRug);
    }

    public void returnPolicy() throws InterruptedException {
        clickByXpath(objOfCustomerService.returnPolicyXPath);
        Thread.sleep(2000);
    }

    public void returnPolicyScreenshot() throws InterruptedException {
        try{
            String text = "Standard Return Policy";
            String actualText = (driver.findElement(By.xpath(objOfCustomerService.returnPolicyText))).getText();

            if (actualText == text){
                System.out.println(actualText == text);
            } else {
                clickByXpath(objOfCustomerService.endOfReturnPolicy);
                take_screenshot("_ "+ actualText + "_" + getTodaysDate());
            }
        }
        catch (Exception noMatch){
            System.out.println("Standard Return Policy Not Found" + noMatch);
        }
    }

}
