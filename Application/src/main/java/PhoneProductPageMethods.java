import org.openqa.selenium.By;
import org.testng.Assert;
import pageobjects.objOfHomePage;
import pageobjects.objOfPhoneProductsPg;

import java.io.IOException;


public class PhoneProductPageMethods extends Utility{
    //6 tests

// Search Text Box Links are on homepage

    String dataFilePath="C:\\Users\\Wifey\\Dropbox\\Newegg\\Application\\src\\test\\Data\\DDT-Newegg.xlsx";
    String phoneProduct = readFromExcel(dataFilePath, "DDT-Newegg", "B2");
    String ZipCode=readFromExcel(dataFilePath, "DDT-Newegg", "B4");

    public PhoneProductPageMethods()throws IOException {}

    public void phoneSearch() throws InterruptedException {


        typeTextByXpath(objOfHomePage.searchTextBoxXpath,phoneProduct);

    clickByXpath(objOfHomePage.searchButtonXpath);


}

    public void validateProductPage() throws InterruptedException {

        phoneSearch();

        String titlePage= driver.findElement(By.className("page-title-text")).getText();

        Assert.assertEquals(titlePage.toLowerCase(), "\"samsung galaxy s9 plus\"");

    }

    public void selectProduct() throws InterruptedException {

        take_screenshot("ProductpageScreensots" +getTodaysDate());
        clickByXpath(objOfPhoneProductsPg.productDetailsXpath);
         Boolean text= driver.findElement(By.xpath(objOfPhoneProductsPg.productDecriptionTextXpath)).getText().contains("Samsung Galaxy S9+ Plus");
         System.out.println(" The page includes the correct details of the product : " + text);


    }


    public void addToCart() throws InterruptedException {

        clickByXpath(objOfPhoneProductsPg.addToCartXpath);

        clickByXpath(objOfPhoneProductsPg.noThanksXpath);

    }
    public void addZipCode() throws InterruptedException {

        phoneSearch();

        selectProduct();

        addToCart();

        typeTextByXpath(objOfPhoneProductsPg.enterZip,ZipCode);

        clickByXpath(objOfPhoneProductsPg.ZipUpdateXpath);

    }

    public void checkOut () throws InterruptedException {

        addZipCode();

        clickByXpath(objOfPhoneProductsPg.checkOutXpath);

    }



}