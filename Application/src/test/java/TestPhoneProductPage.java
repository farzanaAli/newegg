import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.io.IOException;

public class TestPhoneProductPage extends PhoneProductPageMethods {

    // this class will call the methods
    public TestPhoneProductPage() throws IOException {
    }

        @Test (priority = 14, enabled = false)

        public void testSearchBox() throws InterruptedException, IOException {

            phoneSearch();
        }


        @Test (priority =15, enabled = false)

        public void testproductpage() throws InterruptedException {

            validateProductPage();
        }

        @Test (priority =16, enabled = false)

         public void testsProductFromSuggestions() throws InterruptedException {

            phoneSearch();

            selectProduct();

        }


        @Test (priority =17, enabled = false)

        public void testAddToCart ()throws InterruptedException {

            phoneSearch();

            selectProduct();

            addToCart();

        }

        @Test (priority =18, enabled = false)


        public void testBoxZipCode () throws InterruptedException {

            addZipCode();

        }

        @Test (priority =19, enabled = true)

        public void testCheckOutButton () throws InterruptedException {

            checkOut();
        }



    }






