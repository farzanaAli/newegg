import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class TestCustomerService extends CustomerServiceMethods {


    public TestCustomerService()throws IOException {}
    @Test (priority = 20, enabled = false)
    public void testTrackOrder ()throws InterruptedException{
        trackOrder();
        signIn();
    }

    @Test (priority = 21, enabled = false)
    public void testFindInvoice ()throws InterruptedException{
        findInvoice();
        signIn();
        invoiceOptions();
        signOut();
    }

    @Test (priority = 22, enabled = false)
    public void testReturnItem ()throws InterruptedException{
        returnItem();
        signIn();
    }

    @Test (priority = 23, enabled = true)
    public void testReturnStatus ()throws InterruptedException{
        returnStatus();
        signIn();
        returnSAssert();
    }

    @Test (priority =24, enabled = false)
    public void testFindRebates ()throws InterruptedException{
        findRebates();
        rebateOptions();
    }

    @Test (priority = 25, enabled = false)
    public void testReturnPolicy ()throws InterruptedException{
        returnPolicy();
        returnPolicyScreenshot();
    }

}
