import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class TestLogInPage extends LogInPageMethods {

    // this class will call the methods

    public TestLogInPage()throws IOException {}
    @Test (priority = 7, enabled = false)
    public void testLoginPage() throws InterruptedException {
        verifyLoginLink();  //Test 1, verifies if login link works
    }

    @Test (priority =8, enabled = false)
    public void testloginBoxFunctionality() throws InterruptedException {
        goToLogin();
        testLoginBox();
    }

    @Test (priority = 9, enabled = false)
    public void testloginFunctionality() throws InterruptedException {
        goToLogin();
        testLoginBox();
        testLoginButton();
    }

    @Test (priority = 10, enabled = false)
    public void testlogoutFunctionality() throws InterruptedException {
        goToLogin();
        testLoginBox();
        testLoginButton();
        testLogoutButton();
    }

    @Test (priority = 11, enabled = false)
    public void testsignUpFunctionality() throws InterruptedException {
        verifySignUpFunction();
    }

    @Test (priority = 12, enabled = false)
    public void testloginFailureFunctionality() throws InterruptedException {
        testLoginFailure();
    }

    @Test (priority = 13, enabled = true)
    public void testDataFromExcel()throws InterruptedException{
        getDataFromExcel();
    }




}
