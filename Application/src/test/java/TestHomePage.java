
import org.testng.annotations.Test;


public class TestHomePage extends HomePageMethods {

  // this class will call the methods

  @Test (priority = 1,enabled = true)
  public void testElements () throws InterruptedException {

     captureNeweggLogoElement();
  }

  @Test (priority = 2,enabled = false)
  public void testingDropDownLinks ()throws InterruptedException{

    testDropDownNeweggMobileLink ();
    testDailyDealsLink();
  }

  @Test (priority = 3,enabled = false)
  public void testScreenshotIMethods () throws InterruptedException{

    testDealsAndServicesTab();
  }

  @Test (priority = 4, enabled = false)
  public void testAssert ()throws InterruptedException{

    assertDropDownEmailDealsLinks();
  }

  @Test (priority = 5,enabled = false)
  public void testBusinessLink()throws InterruptedException{

    testNeweggBusinessLink();
  }

  @Test (priority = 6,enabled = false)
  public void testingThroughDropDownMenu() throws InterruptedException{
    testDropdownElementOfDealsAndServices();
  }



}
