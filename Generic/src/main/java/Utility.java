import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.StringTokenizer;


public class Utility extends Base {
    public void takeScreenshotOfElement(String elementXpath, String nameOfElement, String actualLocation) throws InterruptedException {


        String obj = clickXpath(elementXpath);
        WebElement logoName = driver.findElement(By.xpath(obj));
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        BufferedImage fullImg = null;
        try {
            fullImg = ImageIO.read(screenshot);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Get the location of element on the page
        Point point = logoName.getLocation();
        // Get width and height of the element
        int eleWidth = logoName.getSize().getWidth();
        int eleHeight = logoName.getSize().getHeight();
        // Crop the entire page screenshot to get only element screenshot
        BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(),
                eleWidth, eleHeight);
        try {
            ImageIO.write(eleScreenshot, "png", screenshot);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Copy the element screenshot to disk
        String pimaryLocation = "C:\\images\\" + nameOfElement + ".png";
        File screenshotLocation = new File(pimaryLocation);
        try {
            FileUtils.copyFile(screenshot, screenshotLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }

        File srcFile = new File(pimaryLocation);  // path + filename
        File destDir = new File(actualLocation + getTodaysDate()); // path only
        try {
            FileUtils.copyFileToDirectory(srcFile, destDir);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void clickByCasSelector(String CssSelector) throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.cssSelector(CssSelector)).click();

    }
//    File file = new File("D:/data.csv");
//        if(file.exists()){
//        System.out.println("File Exists");
//    }
//    BufferedReader bufRdr;
//    bufRdr = new BufferedReader(new FileReader(file));
//    String line = null;
//
//        while((line = bufRdr.readLine()) != null){
//        StringTokenizer st = new StringTokenizer(line,",");
//        col=0;
//        while (st.hasMoreTokens()){
//            //Following stroing the data of csv
//            numbers[row][col] = st.nextToken();
//            col++;
//        }
//        System.out.println();
//        row++;
//    }
//        bufRdr.close();
//
//
//}
 String dataFilePath="C:\\Users\\Wifey\\Dropbox\\Newegg\\Application\\src\\test\\Data\\DDT-Newegg.xlsx";
}
