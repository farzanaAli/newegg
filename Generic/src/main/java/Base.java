import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.WebDriver;
import java.net.MalformedURLException;
import java.net.URL;





public class Base {

    public String ChromeDriver =

            //"C:\\Users\\tchow656\\Desktop\\NewEgg\\Generic\\src\\Driver\\chromedriver.exe";


            //"C:\\Users\\Azwad\\Desktop\\new_newegg\\Generic\\src\\Driver\\chromedriver.exe";
            //"C:\\Users\\OSMAN\\Desktop\\PROJECTSDET\\Generic\\src\\Driver\\chromedriver.exe";
            "C:\\Users\\Wifey\\IdeaProjects\\BBC\\Generic\\src\\Driver\\chromedriver.exe";
    //Farzana-everyone should have a different path

    public WebDriver driver = null;
    public Logger log = Logger.getLogger(Base.class.getName());

    @Parameters({"useSauceLab", "userName", "key", "appUrl", "os", "browserName", "browserVersion", "browserStack"})
    @BeforeMethod
    public void setUp(boolean useSauceLab, String userName, String key, String appUrl, String os, String browserName, String browserVersion, boolean browserStack) throws IOException {
        if (useSauceLab == true) {
            //getSauceLabDriver(userName, key, os, browserName, browserVersion);
        } else if (browserStack == true) {
            getBrowserStackDriver(userName, key, os, browserName, browserVersion);
        } else {
            getLocalDriver(os, browserName);
        }


        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.navigate().to(appUrl);
        driver.manage().window().maximize();
        log.info("browser loaded with App");
    }

    private WebDriver getBrowserStackDriver(String userName, String key, String os, String browserName, String browserVersion) throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("Platform", os);
        capabilities.setBrowserName(browserName);
        capabilities.setCapability("version", browserVersion);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver = new RemoteWebDriver(new URL("https://" + userName + ":" + key + "@hub-cloud.browserstack.com/wd/hub"), capabilities);

        return driver;
    }

    @AfterMethod
    public void cleanUp() throws InterruptedException {
        log.info("driver is quiting");
        driver.quit();
        System.out.println();
    }

    //get local driver
    public WebDriver getLocalDriver(String os, String browserName) {
        if (browserName.equalsIgnoreCase("firefox")) {
            //driver = new FirefoxDriver();
        } else if (browserName.equalsIgnoreCase("chrome")) {
            if (os.equalsIgnoreCase("windows")) {
                System.setProperty("webdriver.chrome.driver", ChromeDriver);
                driver = new ChromeDriver();
            } else {
                System.setProperty("webdriver.chrome.driver", ChromeDriver);
            }
            driver = new ChromeDriver();
        } else if (browserName.equalsIgnoreCase("safari")) {
            //driver = new SafariDriver();
        } else if (browserName.equalsIgnoreCase("ie")) {
            System.setProperty("webdriver.ie.driver", "");
            //driver = new InternetExplorerDriver();
        } else if (browserName.equalsIgnoreCase("htmlunit")) {
            //driver = new HtmlUnitDriver();
        }
        return driver;

    }

    public String getTextByXpath(String Xpath) throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath(Xpath)).getText();

        return Xpath;
    }

    public void typeTextByXpath(String Xpath, String input) throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath(Xpath)).sendKeys(input);
    }


    public void clickByXpath(String Xpath) throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath(Xpath)).click();

    }

    public String clickXpath(String Xpath) throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath(Xpath)).click();
        return Xpath;
    }

    public void take_screenshot(String screenShotName) throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        try {

            Thread.sleep(3000);
            //FileUtils.copyFile(src, new File("C:\\Users\\OSMAN\\Desktop\\PROJECTSDET\\Application\\src\\test\\ScreenshotsOrImages\\ProductPageScreenShot\\" + screenShotName + ".png"));
            //FileUtils.copyFile(src, new File("C:\\Users\\tchow656\\Desktop\\NewEgg\\Application\\src\\test\\ScreenshotsOrImages\\CustomerServiceScreenshots\\" + screenShotName + ".png"));
            //FileUtils.copyFile(src, new File("C:\\Users\\Azwad\\Desktop\\new_newegg\\Application\\src\\test\\ScreenshotsOrImages\\LoginPageScreenshots\\" + screenShotName + ".png"));
            FileUtils.copyFile(src, new File("C:\\Users\\Wifey\\Dropbox\\Newegg\\Application\\src\\test\\ScreenshotsOrImages\\HomepageScreenshots\\_" + screenShotName + ".png"));

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public String getTodaysDate() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        return timeStamp;
    }

    public static String readFromExcel(String fileRef, String sheetRef, String cellRef) throws IOException {
        FileInputStream fis = new FileInputStream(fileRef);
        Workbook wb = new XSSFWorkbook(fis);
        Sheet sheet = wb.getSheet(sheetRef);
        DataFormatter formatter = new DataFormatter();
        CellReference cellReference = new CellReference(cellRef);
        Row row = sheet.getRow(cellReference.getRow());
        Cell cell = row.getCell(cellReference.getCol());
        String value = "";
        if (cell != null) {
            value = formatter.formatCellValue(cell);
        }
        return value;

    }
}
